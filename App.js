import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, SafeAreaView,View } from 'react-native';
import Tabbar from './src/components/Tabbar';
import Header from './src/components/Header';
import NewsFeed from './src/screens';
import {store} from './src/store/store'
import { Provider } from 'react-redux';
export default function App() {
 
  return (
    <Provider store={store}>
    <View style={styles.container} >
      <Header />
    <View>
    <Tabbar/>
    </View>
   
    <NewsFeed/>
    </View>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop:10
  },
});