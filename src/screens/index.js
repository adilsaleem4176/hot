import React,{useEffect}from 'react'
import { StyleSheet, Text, FlatList } from 'react-native'
import Feed from '../components/NewsFeedComponent'
import {useSelector,useDispatch} from 'react-redux'
import {GET_POSTS} from '../store/actions'
const NewsFeed = () => {
    const dispatch = useDispatch();
    useEffect(() => {
     dispatch({
         type:GET_POSTS
     })
    }, [])
    const data = useSelector(state => state.Post)
    return (
        <FlatList
            numColumns={3}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            data={data.data}
            keyExtractor={item => item.id.toString()}
            renderItem={({ item }) => <Feed image={`https://picsum.photos/id/${Math.floor(Math.random() * (1000 - 1) + 1)}/200/300` } item={item} />}
        />

    );
}

export default NewsFeed

const styles = StyleSheet.create({})
