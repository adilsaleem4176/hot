import { fork,all } from '@redux-saga/core/effects'
import {actionPostsWatcher} from './Posts'
export function* rootSaga() {
    yield all([
        fork(actionPostsWatcher)
    ])
}