import {GET_POSTS,GET_POSTS_RECEIVED} from '../../actions'
import {takeLatest,put } from '@redux-saga/core/effects'
import {requestGet,API} from '../../../api'

function* getPosts(){
    try {
        const response=yield requestGet(API.GET_POST)
                yield put({
                    type:GET_POSTS_RECEIVED,
                    payload:response
                })
    } catch (error) {
        console.log('error in getPosts',error);
    }
    
}

export function* actionPostsWatcher() {
    yield takeLatest(GET_POSTS, getPosts);
}

