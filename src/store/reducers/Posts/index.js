import { GET_POSTS, GET_POSTS_RECEIVED } from "../../actions";
const posts = (state = {}, action) => {
  switch (action.type) {
    case GET_POSTS:
      return { ...state, loading: true };
    case GET_POSTS_RECEIVED:
      return { ...state, loading: false, data:action.payload };
    default:
      return state;
  }
};
export default posts;
