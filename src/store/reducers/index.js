import Post from './Posts'
import {combineReducers} from 'redux'
const rootReducer = combineReducers({
    Post
});

export default rootReducer;
